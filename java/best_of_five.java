import java.util.Random;
import java.util.Scanner;
import static java.lang.System.out;

class BestOfFive {

    public static void main(String[] args) {
	
	Scanner cin = new Scanner(System.in);
	Random rand = new Random();
	int guess;
        int target;
	
	out.println("Guessing game");
	target = rand.nextInt(5) + 1;
	boolean guessed = false;
	while(!guessed) {
	    out.print("Enter a guess between 1 and 5 inclusive: ");
	    guess = cin.nextInt();
	    if(guess < target){
		out.println("Higher, try again");
	    } else if(guess > target){
		out.println("Lower, try again");
	    } else {
		guessed = true;
		out.println("You got it!!\n");
		out.println("The number is " + target);
	    }
	}   
    }
}
