#include <ctime>
#include <cstdlib>
#include <iostream>

using namespace std;

int main(){

  cout << "Guessing game" << endl << endl;

  int guess;
  srand(std::time(nullptr));
  int target = (rand() % 5) + 1;
  bool guessed = false;
  while(!guessed){
    cout << "Enter a guess between 1 and 5 inclusive: ";
    cin >> guess;
    if(guess < target) {
      cout << "Higher, try again" << endl;
    } else if(guess > target){
      cout << "Lower, try again" << endl;
    } else {
      guessed = true;
      cout << "You got it!!" << endl << endl;
      cout << "The number is " << target << endl << endl;
    }
  }
}
