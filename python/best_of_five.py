from random import randint


print("Guessing game")
print()

target = randint(1,5)
guessed = False

while not guessed:
    print("Enter a guess between 1 and 5 inclusive: ", end = '')
    guess = int(input())
    print()
    if guess < target:
        print("Higher, try again")
    elif guess > target:
        print("Lower, try again")
    else:
        guessed = True
        print("You got it!!")
        print()
        print(f"The number is {target}")

        
