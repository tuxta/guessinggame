program BestOfFive;

var target: integer;
var guessed: boolean;
var guess: integer;
    
begin
  writeln('Guessing game');
  writeln();
  writeln();
  randomize();
  target := random(5) + 1;
  guessed := FALSE;
  while not (guessed) do
    begin
      write('Enter a guess between 1 and 5 inclusive: ');
      readln(guess);
      writeln();
      if (guess < target) then
        writeln('Higher, try again')
      else if(guess > target) then
        writeln('Lower, try again')
      else
        begin
	  guessed := TRUE;
	  writeln();
          writeln('You got it!!');
	  writeln();
	end;
    end;

end.
