using System;

public class BestOfFive
{
    public static void Main(string[] args)
    {
        int guess;
	int target;
	
        Console.WriteLine("Guessing game");
	target = 4;
	bool guessed = false;
	while(!guessed){
	    Console.WriteLine("Enter a guess between 1 and 5 inclusive: ");
	    guess = Convert.ToInt32(Console.ReadLine());
	    if(guess < target){
	        Console.WriteLine("Higher, try again");
	    } else if(guess > target) {
	        Console.WriteLine("Lower, try again");
	    } else {
	        guessed = true;
		Console.WriteLine("You got it!!\n");
	        Console.WriteLine("The number is " + target);
	    }
	}
    }
}
