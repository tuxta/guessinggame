use strict;
use warnings;

print "Guessing game\n\n";

my $target = int(rand(5)) + 1;
my $guessed = 0;

while(!$guessed) {
    print "Enter a guess between 1 and 5 inclusive: ";
    my $guess = int(<STDIN>);
    print "\n";
    if($guess < $target) {
	print "Higher, try again\n";
    } elsif($guess > $target){
	print "Lower, try again\n";
    } else {
	print "You got it!!\n\n";
	print "The number is ${target}\n";
	$guessed = 1;
    }
}

