#include <time.h>
#include <stdlib.h>
#include <stdio.h>

int main(){

  puts("Guessing game\n\n");

  int guess;
  srand(time(NULL));
  int target = (rand() % 5) + 1;
  int guessed = 0;
  while(!guessed){
    puts("Enter a guess between 1 and 5 inclusive: ");
    scanf("%d", &guess);
    if(guess < target) {
      puts("Higher, try again\n");
    } else if(guess > target){
      puts("Lower, try again\n");
    } else {
      guessed = 1;
      puts("You got it!!\n\n");;
      printf("The number is %d\n\n", target);
    }
  }
}
