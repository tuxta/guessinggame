<?php

echo "Guessing game\n\n";

$target = random_int(1, 5);
$guessed = false;

while(!$guessed) {
    echo "Enter a guess between 1 and 5 inclusive: ";
    $guess = intval(readline());
    echo "\n";
    if($guess < $target){
        echo "Higher, try again\n";
    } elseif($guess > $target){
        echo "Lower, try again\n";
    } else {
        $guessed = true;
	echo "You got it!!\n\n";
	echo "The number is ".$target."\n";
    }
}
